const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const compression = require("compression");
const cors = require("cors");

const contextService = require("request-context");

const options = {
    allowedHeaders: ["Origin", "Content-Type", "Accept"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "*",
    preflightContinue: false
};

const router = require("./route/api");

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors(options));
app.options("*", cors(options));

// Imp: Registering context as PER REQUEST
app.use(contextService.middleware("request"));

app.use("/api", router);

const port = process.env.PORT_NO || 3200;

app.get("/", (req, res) => {
    res.send("Service is running...");
});

app.listen(port);
console.log("Server is started on port " + port + "...");