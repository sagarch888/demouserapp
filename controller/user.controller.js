const UserService = require("../service/user");

const contextService = require("request-context");

class UserController {

    register(app) {
        /**
         * Setting the userList into `request-context` and returning the same
         */
        app.route("/getUserList")
            .get((request, response) => {
               const userList = {
                   "userOne": "NewuserOne",
                   "userTwo": "NewuserTwo"
               }
               // Setting the userList in namespace `request` against `targetuserData` key.
               contextService.set("request:targetuserData", userList);
               const userService = new UserService();
               const userResponse =  userService.getAllUserList();
               return response.json(userResponse);
            });
    }
}

module.exports = UserController;