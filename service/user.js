const contextService = require("request-context");

class UserBiz {

    /**
     * Get all the active user details
     */
    getAllUserList() {
        try {
            // Reading from request-context with request `namespace` and `targetuserData` key
            const userList = contextService.get("request:targetuserData");
            return userList;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = UserBiz;